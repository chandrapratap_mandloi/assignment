var global= /^[A-Za-z]+$/;
// First Name
$(document).ready(function()
{
  function registration()
  {
    if( fname()==false){return false;}
    if( lastname()==false){return false;}
    if( phone()==false){return false;}
    if( office()==false){return false;}
    if( email()==false){return false;}
    if( password()==false){return false;}
    if( cnpassword()==false){return false;}
    if( calc()==false){return false;}
    if (gender()==false) {return false;}
    if (intrest()==false) {return false;}
    if (about()==false) {return false;}
  }  
  function fname()
  {
    var name = $("#fname").val();
    // var name= $('#fname').val();
    if(name.length===0)
    {
       $("#fn").html("Mandatory Field");
       return false;
    }
     else
    {
      $('#fn').html(" ");
    }
    if(name.match(global))
    {
      return true;
    }
    else
    {
      $('#fn').html("Enter Characters Only");
      return false;
    }
  }

  function lastname()
  {
    var lname = $("#lastname").val();
    var name= $('#lastname').val();
    console.log(name)
    var b=Number(name.length);
    console.log(b);
    if(b==0)
    {
       $("#ln").html("Mandatory Field");
       return false;
    }
    else
    {
      $('#ln').html(" ");
    }
    if(lname.match(global))
    {
      true;
    }
    else
    {
      $('#ln').html("Enter Characters Only");
      return false;
    }
  }

  function phone()
  {
    var num = $('#phone').val();
    var b=Number(num.length);
    console.log(b);
    if(b===0)
    {
      console.log("hello");
      $("#ph").html("Mandatory Field");
      return false;
    }
    if (isNaN(num))
    {  
      $("#ph").html("Enter Numeric value only");  
      return false;  
    }
    else
    {
      $("#ph").html("");
    } 
    if(num.startsWith("+")) {
      $("#phone").maxLength = 13;
      if (num.length==13) 
      {
        $("#ph").html(""); 
      }
      else
      {
        $("#ph").html("Enter 12 Number");
        return false;  
      }
        return true;
    }
    else 
    {
      $("#phone").maxLength = 10;
      if (num.length==10) 
      {
        $("#ph").html(""); 
      }
      else{
      $("#ph").html("Enter 10 Number");
      return false;  
    }
    }
  }

  function office()
  {
    var off = $("#office").val();
    var b=Number(off.length);
    if(b===0)
    {
      $("#num").html("Mandatory Field");
      return false;
    }
    if (isNaN(off))
    {  
      $("#num").html("Enter Numeric value only");  
      return false;  
    }
    else
    {
      $('#num').html(" ");  
    }
  }

  function email()
  {
    var a = $('#email').val();
    var pattern = /^\S\D\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/; 
    var pat=/^\D\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}?\.[a-zA-Z]{2}$/;
    var pa = /^\D\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/; 
    var b=Number(a.length);
    if(b===0)
    {
      $("#ml").html("Mandatory Field");
      return false;
    }
    else
    {
      $('#ml').html(""); 
    }
    if(a.match(pa) && a.match(pattern) || a.match(pat))
    {
      //return true;
      // document.getElementById('ml').innerHTML="Valid";
    }
    else
    {
      $('#ml').html("Enter correct mail id");
      return false;
    }
  }

  function password()
  {
    var pass = $("#password").val();
    var c=/(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,12})$/;
    var letter = /[a-zA-Z]/; 
    var number = /[0-9]/;
    var b=Number(pass.length);
    if(b===0)
    {
      $("#pass").html("Mandatory Field");
      return false;
    }
    else
    {
      $('#pass').html(" "); 
    }
    if(pass.match(letter) && pass.match(number))
    {
      $('#pass').html("");
    }
    else
    {
      $('#pass').html("Enter Alphanumeric Value Only");
      return false;
    }  
    if(pass.length <= 12 && pass.length >= 8)
    {
      $('#pass').html("");
    }
    else
    {
      $('#pass').html("make sure the input is between 8-12 characters long");
      return false;  
    }
  }

  function cnpassword()
  {
    var a = $('#password').val();
    var b = $('#cnpassword').val();
    var c=Number(a.length);
    if(c===0)
    {
      $("#cnpass").html("Mandatory Field");
      return false;
    }
    else if(a==b)
    {  
      $("#cnpass").html("");
      // return false;
    }  
    else
    {  
      $('#cnpass').html("Password does not match");
      return false;  
    }
  }

  function calc()
  {
    var date=new Date;
    var birthMonth=$('#month').val();
    var birthYear=$('#year').val();
    var birthDay=$('#day').val();
    // console.log(birthDay);
    // console.log(birthYear);
    // console.log(birthMonth);
    if((birthMonth==('Month'))||(birthYear==('Year'))||(birthDay==('Day')))
    {
      $("#dobMsg").html("Select DOB");
      return false;
    }
    else
    {
      $("#dobMsg").html("");
      var dob=new Date(birthYear,birthMonth/10,birthDay);
      // console.log(birthMonth);
      var m = date.getMonth()+1;
      console.log(m);
      var y = date.getFullYear()-1;
      console.log(y);
      console.log("birthMonth");
      console.log(birthMonth);
      var q = birthMonth-m;
      console.log(q);
      var age = (y-birthYear)+(Math.abs(12-q)/10)
      console.log(age);
      $('#calc').val(age);
      $('calc').disabled = true;
    }

  }

  function gender()
  {
    if($("#residence1").is(":checked")==false && $("#residence2").is(":checked")==false)
    {
      $('#gn').html("Select Gender");
      return false;
    } 
    else {
      $('#gn').html("");
    }
  }

  function intrest()
  {
    if($("#checkbox_sample18").is(":checked")==false && $("#checkbox_sample19").is(":checked")==false && $("#checkbox_sample20").is(":checked")==false)
    {
      $('#cx').html("Select At least one box");
      return false;
    } 
    else {
      $('#cx').html("");
      return true;
    }
  }
   function about()
  {
    var detail=$("#about").val();
    var detaillen=Number(detail.length);
    var a = /.*\S.*/;
    if(detaillen===0)
    {
      $('#au').html("Mandatory Field");
      return false;
    }
    else
    {
      $('#au').html(""); 
    }
    if(detail.match(a))
    {
      return true;
    }
    else
    {
      $('#au').html("Can Not be Blank");
      return false;
    }
  }
  $("#fname").blur(fname);
  $("#lastname").blur(lastname);
  $("#phone").blur(phone);
  $("#office").blur(office);
  $("#email").blur(email);
  $("#password").blur(password);
  $("#cnpassword").blur(cnpassword); 
  $("#month").change(calc); 
  $("#day").change(calc);
  $("#year").change(calc);
  $("#residence1").change(gender);
  $("#residence2").change(gender);
  $("#checkbox_sample18").change(intrest);
  $("#checkbox_sample19").change(intrest);
  $("#checkbox_sample20").change(intrest);
  $("#about").blur(about);
  $("#newbtn").click(registration)

});
